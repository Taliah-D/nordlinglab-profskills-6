This diary file is written by Chi Wei, Hsieh in the course Professional skills for engineering the third industrial revolution.
Chinese name: 謝濟緯
Student ID: I54091149

# 2021-09-23 #

* Still trying to figure out what git is about
* Since electric cars are gonna be a lot cheaper, I'm gonna start investing in Tesla right now

# 2021-09-30 #

* The ability to identify fake news is important
* Never blindly believe nor reject any given information
* The method used to require statistical data has a great impact on the validity and credibility of the results 
* Sources are important as they provide authenticity to data

# 2021-10-07 #

* Loans create money, that's just so cool, I wanna make money too
* Income inequality is terrible, the rich should help the poor more

# 2021-10-14 #

* People who lack the emotional support that they need might go on dark paths
* Ignorance leads to most conflicts
* We should know ourselves better, so that we don't get fooled by extremist propagandas

# 2021-10-21 #

* Doing exercise stimulates the brain to have better function
* Being aware of our physical status allows us to have better performance
* Me as an athlete since a young age can confirm that the above two statements are true in some ways

# 2021-10-28 #

* Many people didn't contribute to the supergroup project, sad
* This week's course is about depression. My ex had depression, I didn't know the right way to deal with that back then. (Plus we had a long time seperated and broke up)
* I like the thought that the way to help depressed people is not by bridging people together, but closing the gap
* Listening and caring is what most depressed and suicidal people need
* Many depressed people see being depressed as being week, hiding their problems from others
* Be yourself, whether you're depressed or not. There is no need to feel bad for who you are

# 2021-11-04 #

* Besides with family, we spend most of our times at school or at work
* Having a good work/school environment is crucial for our well being
* At home we have parents and siblings, at work we have employers and coworkers, at school we have teachers and classmates
* As humans, it is nature for us to want a sense of belonging
* With guidance from mentors and company from friends, it is easier for us to blend into a group
* When encounter problems, we speak with our parents. So when we meet questions at work or school, it is totally fine for us to communicate with our employers or teachers
* Sometimes when we want to change the environment, one way is to change from ourselves

# 2021-11-11 #

* Today's a special holiday!
* I've been extremely busy in the past two weeks, and I'm glad that many affairs are coming to an end by this weekend
* Noise is a serious problem, you don't need to live in an urban neighborhood to experience it, it's everywhere
* A video from one of the group presentations said that noise is subjective, that reminds me of the people singing songs poorly at the dormitory bathroom every night. Their singing is horrible! Yet they're singing so loud!
* I've watched something on TV a few months ago that said something similar to what the professor said about locking CO2 in wooden buildings, it's a cool concept. The TV program also said that some countries are building skyscrapers with wood!

# 2021-11-25 #

* Many people say that we find more and more cases of depression anually because people are more open to the subject. I, however, believe the major cause of it can attributed to the growing problems of society
* There is no denying that social media connections actually creates difficulties in human interactions
* Every day we look into our phones, searching recognition on social media platforms, but ultimately, they are not real connections
* I feel this void deeply everytime I use my phone too much while not speaking to actual people
* For people who were born in this generation of rapid technology grownth, everything comes and go so fast, we start to take many things for granted. People cherish what they have less, to the point they don't know what to cherish
* The best fruits take time to grow. Being honored for hard work and persistence is the best reward ever. Spend more time with people, form bonds, the results must be fruitful

# 2021-12-02 #

* My first time to the classroom! Really impressed by the hardware
* Did a presentation, worked out pretty well
* I have been preached in campus many times by NCKU students who attends the nearby church, I get that they're trying to be friendly, but I feel really annoyed
* Never knew that the church would make noises(probably singing?), they probably don't mean to cause trouble, but it's an issue to be resolved
* Construction noises suck
* Very cool idea the professor gave that we could ask local restaurants rent their cooking spaces to students when they're not open
* I'm tired

# 2021-12-09 #

* Very unhappy about the group project last week, my teammates have no idea what to do and what they did, terrible attitude
* Are news possibly objective? Reminds me of a course I took last semester about whether science is objective. Science is man-defined afterall
* Got mid term on Saturday and I'm very anxious
* Hope everything goes well......

# 2021-12-16 #

* The group assignment was a total mess, group members don't even read messages, no discussion was held before class :(
* Couldn't get on the contribution survey, url problem?
* "Time is the answer", Really liked that. What we see as unchangeable/ absolute right now, might be seen as absurd in a hundred years!
* I'm really curious about how many people in Taiwan are members of gangs 
* Change groups! I totally need that! Hope I pass this course successfully

# 2021-12-23 #

* Last week's team project was probably the smoothest ever! Active member contribution+ fast discussion+ great result
* Many people have lost their jobs due to automation, more jobs will be replaced by machines. It is really critical for the whole society to realize that many essential workers will ultimately be completely obsolete in the future.　
* The fact that government polices favor the rich implies concerning problems. Either the government is ruled by the rich who lies to get into their position, or that the government cannot run independently without being affected by the demands of the rich.
* Honestly, I think we’re going to face an age when the population exceeds job opportunities, even if everyone is equipped with diverse skills and knowledge.
* Good thing that I’m studying medicine, wouldn’t lose my job anytime soon.
* Walked to the other side of the train station to pick up a model kit I pre-ordered at June. The box art loocks totally phenomenal, can't wait to build it after this busy period.
* Went swimming, then ate McDonalds until very late. Watched the last episode of Hawkeye at midnight, unproductive but fun.
* Not a successful day, but not disappointing either.

# 2021-12-24 #

* Too tired in the morning because I slept too little, was sleeping half of the time during the morning course.
* Didn't understand what did profeesor was talking about during the afternoon class, boring as hell, gotta review it again sometime.
* Did passage of my cells in the afternoon while showing it to the newcomer at the lab. Which was kinda fun.
* Washed my clothes and cleaned my room a little, still a bunch of mess tho.
* Went on the train to Taichung, met up with my parents and told them what happened in the past few days.
* Totally love my bed, warm and comfy. Excited for tomorrow's food.
* Not a successful day because I was totally unproductive.

# 2021-12-25 #

* It's Christmas, didn't celebrate it tho. Poor loner me.
* Tomorrow is my father's 60 th birthday, we had a cute birthday cake and wished him a happy new year!
* Been doing a group assignment all day, pretty happy with the progress.
* Not happy with the progress of other courses tho.
* Also, spent too much time on my phone.
* I'm gonna say today's semi-successful because making that video assignment was fun.

# 2021-12-26 #

* My family went to a restaurant that serves grilled beef for my father's birthday. The meat quality was fantastic! Haven't tasted meat that delicious for a long time.
* Went to watch the Matrix Reserructions. It was not bad, but didn't meet my expectations.
* Spent more time on the group assignment video I was working on yesterday. Finally completed it, I'm now terribly exhausted.
* Totally love the result of the video!
* I would say today went pretty well since finishing that video means ending another course's requirements.

# 2021-12-27 #

* Slept till twelve, skipped two important classes, again!
* Still a little sleepy in the afternoon classes, why am I so tired??!!
* Wanted to eat some spicy duck blood but the restaurant wasn't open, sad...
* Had my final test of my student ambassador association training. Some parts of the test was unexpected, and I did quite a lot of mistakes as well. Super nervous of whether I would successfully pass it.
* Cleaned my room better, my roomates must be proud.
* Had a conference on a group project, our progress is really worth concerning...
* Today could've better.

# 2021-12-28 #

* Sleepy all day, was sleeping during lots of classes
* Had that presentation with the video I made last weekend, all three profeesors thought the video was very well done
* Assembled a little part of my unfinsihed model kit
* Went back to dorm and listened to a biochemistry class that I skipped, while assembling legos
* Ate dinner after 9pm with my friends
* Today was totally not successful because I was too unproductive

# 2021-12-29 #

* Slept until 10am
* Skipped all the classes in the morning
* My professor took us to eat hot pot for lunch, was very full
* Went to a cafe, intended to study but didn't
* Ate hot pot again with the student ambassador group
* We had games and other activities afterwards
* Had a meeting at 11pm, super tired
* Today was not successful as well, unproductive
* Successful= Become better= Be productive............?

# 2021-12-30 #

* Sleepy, again
* I think my 5 rules to success was not bad
* Someone said smoke a cigar everyday to be successful, sounds like some cigaratte propaganda
* Didn't bring my suit to wear in class. But I had to go to the swimming pool after class, so...
* It was kinda funny when one group discovered that the cars on the roads make more noise than the church

# 2022-01-06 #

* It's almost the end of the semester! I have 7 tests to take next week.
* I totally support gender equality, but I cannot agree with the notions of many other people regarding the same topic.
* The word "feminism" has been distorted in many occasions, to the point that I believe there should be a change in word use for gender equality. Primarily because there are so many females who believe they are fighting for equality are actually attacking the opposite gender.
* I wouldn't take "I'm a feminist." as a symbol of justice and right any more. Now I would take it as a caution and protective measure that deliberately sperates genders.
* They say it's gender equality to avoid using "man" and instead use "person" in entitlements. But why? The word "man" itself can include both genders. If your goal is to create true gender equality, wouldn't the word "feminism" itself be sexist, because it includes feme-?
* I recognize gender inequality is a serious problem happening right now. But the many measures to solve this problem remains questionable.

# 2022-01-13 #

* Final class of course~
* Reject the first 37% of the people you dated?! What if I don't have any partners any more......
* Being yourself during a relationship is important I think
* Being able to speak freely and actually speak freely is important to maintain any relationship
* Heartbreak withdrawl, I like that thought. Letting go with determination is the key to heal