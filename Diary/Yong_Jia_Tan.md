This diary file is written by Yong Jia Tan 陳勇嘉 E14085066 in the course Professional Skills for Engineering the Third Industrial Revolution.

# 2021-09-23 #

* The second lecture starts with an introduction on conflicts. It allows me to have a better understanding about how conflicts can be organized into different levels.
* Learned about GITHub and how it could be used as a platform to discuss and coordinate work.
* Learned about **Markdown**, which is commonly seen on Whatsapp for my case. Useful as an emphasis on certain terms.
* Acknowledged that there are a lot of objects in our surroundings that follow the rule of exponential growth leads to a world of abundance.

# 2021-09-30 #

* The third lecture placed an emphasis on statistics, which is a useful tool to allow people to know about trends and the world around them.
  - Main problem with statistics is that it could be easily manipulated by only obtaining desirable data or skewing the results of the data to suit certain contradictory conclusions.
  - Everyone should have the ability to differeciate between good data and bad data, not just believing every data that you see. Try to do your own research.
* Fake news is all over the place due to people who are lazy to fact-check the information that they received.
  - The same thing applies to statistics, be vary of any information presented and try to do your own research.
  
# 2021-10-07 #

* The fourth lecture gave a brief introduction on the Financial System, which is mostly based on credit after the abolishment of the Gold Standard by President Franklin D. Roosevelt.
* The current state of the global Financial System is heavily skewed towards the rich, benefiting them as the more money you have, the greater the power to amass more money.
* From the recent 2008 Financial Crisis, we can know the effects of an unregulated banking industry where credit has been used as a tool to make profit, but ended up hurting themselves instead due to mismanagement.
* I found it absurd about the fact that the professor brought up, which is millions of NTD could just appear from nothing by just taking a loan, which shows that this system has a terrible flaw, since it is not regulated by anything solid other than credit.

# 2021-10-14 #

* Populism is on the rise everywhere, especially in the Western world more recently, as people are getting more negative views on the so called "Threats" that would affect their wellbeing.
* Countries like Poland, Germany and Hungary has a sizeable following of populism and nationalistic societies.
* When you don't really know each other, you will be prone to misinformation being spread by intentional people, which is concerning since this would only reinforce their false beliefs onto certain groups.
* Marginalised groups are the groups that should get more care, but with misinformation being spread around they would only recieve hate.
* Malaysia, the country that I'm from, suffers from people not trusting each other, which has hindered it's development of being a power in South East Asia despite having the resources to become one.
* People should try and learn more about each other, so that we can live in a more compassionate world.

# 2021-10-21 #

* Stories are enjoyable for humans, everyone likes a good story! Yet still, there are plenty of stories that are simply made up, since you cannot really fact-check stories that are passed on by mouth.
* Usually stories passed through word of mouth will be altered, and depending in the severity of the alteration, it could cause conflicts.
* Stories are also used by people who has intentions to manipulate people's thinking, hence the rise in populism. Populism is basically the business of selling stories by Politicians.
* Emotions are hard to control, as you would not know when it became uncontrollable. Training your brain into controlling your emotions takes time and effort.
* Realised that in the grown-up world, we should have the ability to manage our emotions better, as more problem would arise as you grow older, that is why people tend to trust people who are more mature.
* Controlling emotions through breathing has always been the go-to advice for people who are getting nervous, yet still after knowing the science behind rhythmic breathing, it changed my mind of thinking breathing as a placebo to control your emotions.

# 2021-10-28 #

* Mental illnesses is something scary since unlike physical wounds, you cannot really determine the damage caused by mental illnesses.
* More and more people are suffering from mental illnesses in this current age due to unsatisfactory life or traumatic incidents. The industrialisation age did not help either since it has caused more competition among members of society to try and earn more to sustain their lifestyles.
* Solving mental illnesses is not an easy task since the human's understanding towards the human brain is still very shallow.
* The best medicine towards mental illnesses such as depression in my opinion is to have someone to talk to, as to let them feel part of society and not feeling lonely.
* The effects of depression are scary since in serious cases it may even lead to suicide, which is very saddening since most of the time the patients themselves don't really want to do that, they are just being manipulated by their emotions at the time.

# 2021-11-04 #

* Humans live in groups, and it is very important for humans to support each other, having each other's back when they needed it the most.
* The marketplace is highly competitive, sometimes people are being pressured to have to learn new knowledge or skills to increase their value. Yet still, sometimes even if you are a good worker, other factors also come into play.
* Factors not related to work such as the atmosphere of the workspace and the job scope of the workers also effect how well a worker can be suitable for the job.
* Yet still, it is too early to be overthink about the future, as self-improvement is much more important compared to be constantly worrying about the future.

# 2021-11-11 #

* Today a lot has been shared on the topic of climate change, and it's effects towards the world.
* Humans are constantly finding ways to combact these effects, including finding alternative ways to do something that is damaging towards the environment.
* Sadly the awareness on how the environment has changed for the past few years is still very low. Thus efforts to raise awareness must be continued to be done in order to gather the efforts of everyone.

# 2021-11-18 #

* The class for this week has been cancelled.

# 2021-11-25 #

* This week's lecture is more about the environment, and the efforts in reducing the damage caused by humans.
* Laws couldn't really make crime disappear, it only serves as a simple structure to ensure society can be run in the best interest of everyone, through the creation of stability.
* No law is perfect, as it is prone to be abused and manipulated.  
* Social Media can be classified as something that is deeply embedded in our modern daily routine. Almost everyone in this day and age has a social media account, used by humans to "connect" with friends.
* The bigger danger of social media is that, it is an efficient way to connect people, but it is also currently being misused as platforms for bigotry and skepticism to flourish. Controversial views often get a lot of reactions from social media users, which sends a message to the system's algorithm to promote more of these messages.
* The whole social media landscape is toxic for now, as the way it is currently being run is literally obtaining people's attention with every trick in the book, and this has caused emotional damage being done without the users knowing about it.
* As a chinese proverb goes, "Things will be develop in the opposite direction when they become extreme", excessive dependence on social media would make someone more vulnerable towards its' damage. People should try and learn how to use social media to their best interest, and not the other way around.

# 2021-12-02 #

* The lecture started with us placing our phones away from us, and I have touched my pocket for 2 times searching for my phone.
* This made me realise how I am unconsciously dependent on my phone, and allows me to rethink about the role of my phone in my daily life. Probably should reduce the time spent on my phone, as too much of a good thing ain't good for anyone.
* Today's lecture also placed emphasis on the various problems faced by NCKU students, and the things that can be done to fix them, makes me wonder the ability of a group of humans.

# 2021-12-09 #

* This week's lecture discussed about the different opinions towards a certain news that would exist due to a variety of factors.
* The 2022 Beijing Winter Olympics diplomatic boycott incident was discussed a lot during the lecture, and through this incident we would understand different opinions from different countries based on their geopolitical stance towards the players in the incident.
* Politics has always been messy, as being portayed in the news, and there is no simple solution towards solving these conflicts.
* The issues brought up by the supergroups makes me wonder about how a simple thought can be transformed into change through cooperation from different sides.
* The concept of planetary boundaries is new for me, and I feel it deserves the same attention with the Sustainable Development Goals being introduced by the United Nations.
* Planetary Boundaries concept offers humankind a guideline to form a better world, as the enviromental issues listed within are suffering due to human activities.

# 2021-12-16 #

* This week's lecture started with discussing the ways that can be taken to control the damage done to the environment so that it could be contained within the suitable Planetary Boundaries. 
* The most effective way to contol the damage towards the environment is to reduce the amount of people, yet still this is not the best way to combat this problem since the effects of a human existing doesn't really translates to a net negative towards the environment. 
* The checks and balances that currently exists such as the economy and the environment should be enough to reduce the number of humans. In my opinion it is better to focus on how to reduce the effects of humans overall towards the environment.
* The Supergroups has presented the plans to solve the problems surfaced. In overall the plans that were brought up were doable, it just need more dedication from us in order to realise it.
* The voting system in the end has made me wonder about the First Past The Post voting system which has made tactical voting more likely and has not been able to present a more complete intention from its voter base. By introducing a ranked opition in polls, we could know more about the true preferences of the voters so that the data obtained would be more useful. 

# 2021-12-23 #

* This week's lecture was based around discussing the various aspects of the Industrial Revolutions, and how it came to be.
* Common scientific laws such as Moore's Law and Wright's Law has predicted the advancements of semiconductors and how these technological advancements are slowly getting cheaper over the years.
* Although the era of the 3rd Industrial Revolution hasn't really ended, as it still affects our life today since it introduces the idea of using automation to smoothen manufacturing processes. The 3rd Industrial Revolution also serves as a basis for the 4th Industrial Revolution as the introduction of the Internet of Things Idea has made automation more viable in the future, which also has improved on work efficiency tremendously.
* Things would balance itself in the end, just like nature. I really do think that as the wealth inequality gap starts to widen, it would only encourage more and more people to do things to revolt against these measures. And what we can learn from historical events, these events would happen again just like in a loop for centuries, and that is human nature.

# Daily Diary Entries #
- A. You need to label each day as either successful/unsuccessful and productive/unproductive.

- B. Analyse why you felt successful or unsuccessful and productive or unproductive.

- C. State one thing you will do different the next day to be more successful or productive.

 
# 2021-12-24 (Friday)
  A. Unsuccessful and Unproductive

  B. I've skipped classes for today, but I have used that time to attend for an interview. I also had a lot of fun joining the Chirstmas Eve celebrations with my friends. 
  
  C. I should have started to arrange my time wisely through listing down my to-do-list.  
 
# 2021-12-25 (Saturday) 
  A. Successful and Productive
  
  B. Today I visited my friend's booth at a cultural carnival, realised that how interesting can cultural relics can be. I have also started working on my reports. 
  
  C. Wake up earlier so that I have more time to do my tasks.  
  
# 2021-12-26 (Sunday) 
  A. Not Successful and Not Productive
  
  B. Stayed in my dorm for the whole day due to the cold weather, only did minimal revision on my studies. 
  
  C. Plan and allocate time for every subject.  
  
# 2021-12-27 (Monday) 
  A. Successful and Productive
  
  B. Did well on my Finals for one subject, and I have finally spent time on fixing my faulty bicycle. 
  
  C. Undergo maintainance for my bicycle frequently to prevent parts from being faulty.  
 
# 2021-12-28 (Tuesday) 
  A. Successful and Productive
  
  B. Studied for my Mechanism Quiz and performed well.
  
  C. Study earlier to avoid being anxious.  

# 2021-12-29 (Wednesday) 
  A. Not Successful and Not Productive
  
  B. Skipped class again due to not being able to wake up on time. 
  
  C. Sleep earlier.  

# 2021-12-30 #

* This week's class started with the presentation of formal attire, and the sharing of the 5 rules for success.
* It has been a long time since I've worn my formal shirt, it's good to bring it out from the closet.
* Everyone's definition of success is different, as can be seen from their choice of rules. Some people focus of continuous self improvement, while some people chooses a more discipline approach. Try and find your own definition of success, since only you know yourself well.
* Mistakes are one of the ways for humans to grow, although some mistakes will affect you in a bad way, after the effects are gone only the wisdom remains.
* People need to take initiative, so that they can bring change.

# 2022-01-06 #

* This week's class started with an introduction about viral topics on the Internet. Buzzfeed was one of the more infamous mentions since the company has been accused of creating "Hot Garbage", which are content that are really similar with their previous postings that they have used to recirculate on the Internet.
* The method they have used to make their posts viral is to understand which kind of content makes people open their posts, sometimes exaggerating or falsifying facts to obtain views, which is called "Clickbaiting". Since the advertisment revenue on their posts derive from the amount of traffic logging onto the website.
* Yet still, by learning their methods of making posts go viral on social media, we can ensure our own posts can also go viral too, to enable more engagement from friends and family.
* Another thing that was brought up is the effects of stigma onto the society. Since having biases towards things are normal for humans due to how the brain works, humans can still try and ensure their biases are not harmful to another group of person, to ensure that we live in a harmonious society.
* Negative biases against woman and the education in the past thousand years has solidified the position of a male in a society as superior. In this far more equal world, this type of biases aren't suppose to exist anymore, since it really hinders the development of woman and supresses the sounds of children. Sometimes we only gain innovation only by listening to the oppressed.

# 2021-01-13 #

* This week's class was the final class for this course, we had the final exam that is mostly about the statistics of the world, which definitely increased my understanding towards the world in general.
* The main topic for the week was relationships, which is hard to manage to be harsh since everyone is an independent body on it's own, no one can definitely know someone perfectly. For most of the time, these depends on many trials and errors to find out the way for you to manage your relationships.
* Clinging onto something that is taxing on your mental health is really damaging for someone, since it makes you place attention on your internal emotional battle, which can affect your daily life. Overthinking makes you lose connection with the real world. 
* We are all humans after all, and mistakes are bound to happen, thus seeking internal peace and being positive everyday is really crucial for making your life much more happier. Try to divert your attention from things you couldn't control and try to take hold on things you still have.

# Course Objectives #

“A world of facts, challenges, and ideas”

1. Know the current world demographics

* Watch a lot of news reports about the incidents that are happening around the world, which is crucial to know how certain nations think about certain issues.

2. Ability to use demographics to explain engineering needs

* The needs of a certain group of people lies beneath Demographics, thus by thinking about the needs of a certain group of people, you can learn about what type of problems needed to be solved.
* Once the problem has been identified, we can apply tools to try and fix the problem.

3. Understand planetary boundaries and the current state

* I wasn't exposed to planetary boundaries, which exposes a lot about the problem that our world is facing today. Widespread of the concept is crucial since it allows people to be more aware of the boundaries, making people think about how they can improve them.
* The planetary boundaries concept should be as widespread as the Global Warming concept, to reach the same influencing effect. 

4. Understand how the current economic system fail to distribute resources

* The current economic system was constructed by rich people and for rich people, by exposing the way banks and governments work with money, it can tell us a lot about the flaws of the current economic system.
* The introduction on how debt works definitely thought me a lot about how money is only based on credit, which is really unstable. Thus financial literacy is really important.

5. Be familiar with future visions and their implications, such as artiEicial intelligence

* People can try and interpet every new discovery or things that have existed for a long time, and think about the methods that it could be used to enhance the world today. Most of the time new innovations rely on the constant discovery of new uses for old things.
* Follow up on news reports are also important since it exposes us about more new discoveries.
* There are a lot of educational videos on really popular topics such as Blockchain and Artificial Intelligence, thus these videos should be considered as a more fun and entertaining way to learn more abot this new discoveries.


“Personal health, happiness, and society”

6. Understand how data and engineering enable a healthier life

* Data provides us with the signs and symptoms on certain topics, trends can be discovered which would allow us to better prepare ourselves for the future.
* Engineering in a literal sense is just trying multiple methods to solve issues in real life, thus by realising the methods being used by engineers to solve problems in real life, such as analysis etc. We could have a understanding on how to start on solving one problem.
* Thus by making people think more about certain trends and discuss about what can humans do about it, we can have a better understanding about the whole issue and act accordingly to solve problems.

7. Know that social relationships gives a 50% increased likelihood of survival

* Make people do things individually and in a group, to allow them to realise the importance of collaborating together and thus showing them the importance of social relationships in daily life.
* Humans will be much more happier if they aren't isolated from a society, thus more group activities should be held.

8. Be familiar with depression and mental health issues

* Depression is something that only the person who is suffering from it knows how it feels inside. By exposing the opinions from people who has depression, we can know more about depression in general. 
* Fourms or sharing sessions could be held to facilitate interaction between depression victims and people.

9. Know the optimal algorithm for finding a partner for life

* Finding a partner for life isn't as straight cut as it seemed to be, a lot of effort has to be contributed from both sides to establish a solid foundation for the relationship.
* Starting by knowing your own needs and knowing what you can contribute to your partner, and from there you can have a rough idea on which type of partner are you seeking for. The more you know about yourself, the more you can involve yourself in activities that will let you know the people who are compatible for you.

“Professional skills to success”

10. Develop a custom of questioning claims to avoid “fake news”

* Present how news can be altered by a simple misuse of words or how news can be perceived in a false way through certain angles. Provide examples for such fake news so that people can be more aware about how to look for the signs of a fake news article.
* Ask questions about the motive of the article, and ask questions about the news to find out the flaws of the article until the article is proven to be credible.

11. Be able to do basic analysis and interpretation of time series data

* Gather data on things around us, and try to analyse it. Since you are familiar with the thing that you are analysing, you will know what kind of information that you needed to obtain. Plus you can understand the steps for the interpretation. 

12. Experience of collaborative and problem based learning

* Have a group of people to sit around and discuss the problems that they are facing or issues that they have noticed, and try to think of solutions towards the problems that have been mentioned. 
* The discussion of the issues can allow for the exchange of ideas and thus encourages them to think more critically to solve the issue.

13. Understand that professional success depends on social skills

* Connections form from social interactions, and these connections might benefit you in a way in the future. Social skills are the bridge between you and your future friend. People have to realise that everyone has their own strengths and weaknesses, people can provide their skills to help you in certain ways.
* Try and join more social events, so that you can increase your social skills in order to make you more comfortable with the idea of mingling with other people, which might help you in your professional pathway one day. 

14. Know that the culture of the work place affect performance

* Peer pressure is one of those factors that will affect people to become more motivated to increase their productivity. By introducing a competition to a group of people, they will always look at people beside them to ensure they did not fall behind. The same phychological tactic is being used in workspace, when every employee is hardworking, it doesn't make sense to slack around during work.

