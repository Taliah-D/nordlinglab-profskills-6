This diary file is written by Janice Low B24095913 of the course Professional skills for engineering the third industrial revolution.

# 2021-09-30 #

* This is my first lecture. 
* I am inspired by the videos about fake news.
* I think we are living in a world full of fake news and information.
* The video "3 ways to spot a bad statistic" inpired me to think and identify before believing.
* It is essential to identify the source of the news.
* The source and ways are important when we do researches, prevent creating fake news.

# 2021-10-07 #

* It is fun to learn so much facts about money.
* The course today is a bit hard to understand for me.
* The teacher asked us "how to get millions of ntd legally" when he started the topic, I first thought the answer would be owning a bank, but eventually it is not.
* I started to think of the wealth gap problem today after the lesson. 

# 2021-10-17 #

* I am so into this topic.
* Speaking of fascism, I immediately think of China now a days. 
* It is horrifying to see people being kind of brain washed and have faith in their government blindly.
* As a Hong Konger, I am afraid that Hong Kong is gradually becoming a place filled with Chinese communist fascism.
* For me, the most unexceptable thing of fascism is is destroys people's free mind and culture.

# 2021-10-21 #

* It was shocking to know doing excersise can help growthing brain cells.
* When speaking of how to live healthily, I seldom connect it to the mental health part. 
* Matthias Müllenbeck's speech was inspiring.
* It is an interesting point of view of changing our existing health care systems from “sick care” to true “health care”.
* Although I don't think the technology now a days can offer people all over the world to moniter their health by AI, it is a facinating idea.
* Prevention is always better then cure, people's life expectancy can be longer if the technology and health care systems are mature enough.

# 2021-10-28 #

* I was moved by the video "The Bridge Between Sucide and Life".
* It really takes huge courage to live with depression, I was impressed by the mother mentioned in the video.
* However, though I agree with the idea of do not suffer from your depression in silence, I don't think everyone can be brave like the speaker.
* As one of the classmate being depressed without seeking for professional aids, I feels hard to open up my feelings to a stranger, even I known he/she is to help.
* Sometimes, the process of telling or reminding the wounded past is like getting through everything again, and it hurts.
* It is essential but difficult to seek for help and admit something is going wrong.

# 2021-11-04 #

* Big thanks to the ones shared their experiences in depression.
* Emily Esfahani Smith pointed out that chasing happiness can make people unhappy in her talk, which is so sarcastic but true.
* I have been reflecting on my life after the lesson.
* It is a fun process to think what makes me happy in my life and what is the purpose of it.

# 2021-11-11 #

* It was fun to work with groupmates from different countries, knowing the eco-friendly systems from other places.
* Duting the project, we found that the Taiwan government actually did a lot in compensating local Co2 emmisions.
* It was mid-term, so it is hard to find time to do more researches and dig deep into the topic.
* I was not feeling fine for 2 of my groupmates giving no ideas when we were having the onlinemeeting, they didn't even speak until we call their names and devided their parts. 

# 2021-11-25 #

* The video about smartphone addiction is inspiring.
* I think I am quite addicted to smartphones and computers too.
* It is the first time I have the physical class for this lesson.
* I still prefer online lessons though the physical lesson allows us to have group discussions in class.

# 2021-12-02 #

* I couldn't go to the class physically this time.
* I was shocked when professor told everyone in the classroom to hand in their phones.
* I have imagined what if I am in the classroom, how many times would I look at my phone, and the answer should be more than 20 times.
* Every group's ideas are great, but not Implementable for students, as well as my group's.
* It would be great if some companies would be able to know some of our ideas and implement them.

# 2021-12-09 #

* I was absent this week.
* Hope to be able to attend the class physically next time.

# 2021-12-16 #

* The ideas presented by the groups were fascinating.
* The ones I remembered the most should be stop burning paper money and the noise from the church.
* These ideas contains the crush between traditional thinkings and modern concerns.
* There was a rearrangement among groups and I changed to group8.

# 2021-12-16 #

* I had my injection so I was absent again this week.
* Our group has decided to divide our presentation chances evenly, so each of us would have a chance to present.
* The one presents will finish the powerpoint alone, I don't really think it is a good idea, but it is convenient.

# 2021-12-30 #

* I felt sick these 2 days so I finish this diary late.
* The groups seemed not having really much progress.
* It was nice to know this is not the last presentation as I just presented 2 times.

# 2022-01-06 #

* It is the end of the term, and I have been busy studying.
* I really like the idea of investing ladies mentioned in the video played in the lesson.
* I did the presentation this week, this is my first physical presentation in this class.
* I was kind of nervous but it shall be fine.

# 2022-01-13 #

* I studied the wrong material and did a bad job in the. exam.
* I am a bit sad because I joined the course in the third week, so I never met this test before.
* The talks are inspiring again this week, I like the one about fixing a broken heart.
* It is really hard to do so, but I think the idea of keeping a note in the phone is an easy and useful way.