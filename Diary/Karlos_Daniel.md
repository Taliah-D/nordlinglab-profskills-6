This diary file is written by Karlos Daniel E14085147 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #
* In the second lecture, the Professor explain more about conflict, that the conflict are divided into a several level. 
* I can learn how to use bitbucket, even though I have a little bit problem to use it for the first time.
* From the TED talk video by Steven Pinker about "Is the world getting better or worse? A look at the numbers". I got that, the world will getting better in the future.

# 2021-09-30 #
* This week lecture is about fake news.
* learn how to spot a bad statistic. (3 ways: 1. Do you see uncertainty in the data? 2. Do you see yourself in the data? 3. How was the data collected?)
* I feel Mona Chalabi talk too fast, so its hard to understand what she saying. but in the end I got the point.
* My team got a chance to present this week.

# 2021-10-07 #
* This week lecture is about money (economy and finance).
* Money and credit are used for trading or transactions. (consist buyers and seller)
* Learned about how money was determined, the value of money, and about economic growth.
* Filling a questionnaire about money and economy in the world.
* Having a new group for presentation starting from next week.

# 2021-10-14 #
* From today presentation, I learn about the finance from different country which is Taiwan, Malaysia, and Thailand.
* I think making video to presentation it will give us more chance to get score.
* learn more about nationalism and how important it is.
* learn about facism that I never heard before.

# 2021-10-21 #
* In this week, I got chance to read my diary.
* This week lesson is about anxiety and healhty life.
* Learn how anxiety can affect our heart rate and the way we thinking.
* When under pressure, we can't think clearly.

# 2021-10-28 #
* This week, we are change a new group again and create a supergroup.
* Did a supergroup discussion today, but the group was silence and in the end we were late for presentation.
* From the supergroup presentation, knowing that if drink too much water can cause death.
* I think mental illnesses such as depression is a little scary because we cannot determine the damage caused by it.
* In serious cases, depression may cause us to suicide. Even the patient who are suffered did now want to do that, but they are just being manipulated by their emotions at the time.
* I think the best medicine for depression people is someone to talk to. In order word, we need to let them feel society and don't let them feel lonely.

# 2021-11-04 #
* Based on the survey and story from classmate, there was many people got depression or anxiety.
* I also still got confused about Manga X diary, which I didn't know the purpose of the Manga X Diary and also how to use it.
* Learning about work life and let me know about working environment, which is learnt how to deal with toxic work environment.
* Living in good environment, we can have a healthier life and have a better mindset.

# 2021-11-11 #
* This week supposed to be a holiday and no class to attended, but it is postponed to be next week.
* In this week, we watched all of the SuperGroup project presentation. there was many group presentation we watch until the class is over.
* I think there was some interesting topics from the SuperGroup project presentation which is reduce noise to reduce stress, how to prevent CO2 emissions, and about enough sleep to improve our memory or learning.
* Sleep was most important part in our studies, as enough sleep can helps us focus and concentrate in class which mean it can help us to got a beeter score. Sleep is crucial in improving our memory.
* My topics for this SuperGroup project is about find an project to compensate for local CO2 emissions that lead to net zero emissions.
* At first I got a liitle confused with the topics, but after doing an discusiions and searching the information in the internet. Slowly but I can understand about the topics.
* when doing this discussions, we have a a little problem at the communication because there was some people good in English and some people good in Chinese. But luckliy we can understand each other, with some explanation by each member.
* I feel Happy that everything went well.

# 2021-11-25 #
* Today is first physical class (there was online class to) and I attend the physical class.
* I was impressed with the classroom. there was about eight monitors coneected to the Professor laptop which is two in the front, two in the back, and four aside.
* The table position also make us more easy to discussing since the table position was a circle.
* We have a little discussion in the class to make a law and I think that it is not easy to make a law.
* This week topic is about the impact of social media caused.
* We should wise in using social media. IF not, social media can be a toxic things into our life.
* From Jaron Lanier Video about "How we need to remake the internet", Jaron Lanier said that humankind are responsible for the distorted values in nowadays.
* From Simon Sinek video about "Millenials in the workplace", I can relate that how and why there was an indifferent children and also I can relate that social media could make us insecure since social media is very toxic.
* There was a lot of people suicide caused by a toxic social media and some teenagers want not go to school school because of mentally abuse.
* I think that We should doing any activities in order to far away from our phone, maybe doing some exercise, do your hobby, or maybe hangout with friends.
* Keep the relationship in the real world.

# 2021-12-02 #
* This week, we are doing SuperGroup's Project presentation.
* Professor doing a little experiment. We need to put our phone on the Professor table and count how many times we looking for our phone during the class.
* I have learnt about the law in taiwan regarding noise pollution and learn how to respond when there are noise that distrubing us during night time.
* There are no laws regarding underground constructions in Tainan, Taiwan.
* In the end of the class, we are group discussing about an ideas that is simplier which can be implemented before the end of the class.

# 2021-12-09 #
* I attend online class this week because feel a little tired. 
* This week I was doing the presentation alone because my new group member are working with their previous group member, while my previous group member are working with their new group member.
* I think it was not easy to do an presentation alone, especially the topic are about news around the world.
* Even every country's have same news, but every country's news have different stand point.
* Learn about nine planteary boundaries which is biosphere integrity, climate change, novel entities, stratospheric ozone depletion, atmospheric aerosol loading, ocean acidification, biogecochemical flows, freshwater use, and land-system change.
* Substances made by human that negatively affected the environment is pesticide.
* When bees are poisoned by pesticide, there will be no fruit and there will be no food for any living species.

# 2021-12-16 #
* This week is SuperGroup presentation, and I think the method is unique where the presenter from each group are sitting in front and waiting for their turn, and answering the audience question in the end.
* My team just present for a while and the Professor are stopping the presentation because we already done with our topic.
* The audience also have a chance to asking a question that they were curious about.
* We are filling a questionaire about which idea we like, and I choose the idea about "Change Textbooks to PDF file". I choose this file because I think it will more easy to use and more easy to bring anywhere and anytime.

# 2021-12-23 #
* Small Group Presentation with new group member, each group have three minute to do the group presentation.
* Study about dark ages, but I not really understand about dark ages.
* Today debate was divided into three categories of representative which is Capitalist, environmentalist, and working class.
* I felt the debate has interesting topic and I find it funny to listen my classmate debate.
* After hearing the debate, I felt both of them were not wrong because they just want the best. 
* In my opinion, the capatalist are not as evil as we though and the workers just want to have a better life, so that they can bring a better life for the family and for himself.
* I like the statement to not choosing a good company, but coose a good boss. Because good boss will give us more opportunity I think.
* For today, I felt successful and productive day because I woke up at 7.45 AM for attending class. At night, I study for the exam on tuesday.
* For the next day, I will take a rest and be ready for the Christmas.

# 2021-12-24 #
* Today I get a Cupcake and milk from Entrepreneurial though and action class.
* At the night, I have a dinner with a delicious food and after that going to watch Spiderman no way home in cinema.
* For today, I felt unproductive but successful day. Because just watching a movie and have Christmas dinner.
* For the next day, I will study for my exam.

# 2021-12-25 #
* Today is Christmas day.
* I planning to study for my exam.
* My friends are playing basketball, but I did not play because I study for my exam. Actually I really want to play basketball.
* Today I feel very productive and successful because I already study for my exam.
* For the next day, I planning to study again.

# 2021-12-26 #
* Actually today I must go work at night, but because there are not many people in the restaurant. so, the boss said I did not need to come to work today.
* Actually I planning to study after the work, but because I did not have work tonight. So, I change to study for my exam.
* I feel today was an Productive and successful day because i can study for my exam and get more prepared for my exam.
* For the next day, I planning to study again and get prepared for the exam.

# 2021-12-27 #
* I have two exam today which is at 8.10 AM and 15.10 PM. and the exam is going as well.
* At the night, I study again for my exam.
* I feel Productive and successful today because I got study and my exam are going as well.
* For the next day, I have a plan to study again.

# 2021-12-28 #
* I study for my exam.
* Having a exam at 17.10 PM and the exam is going as well. 
* At the night, I have a group discussion for the next monday group presentation.
* I feel productive and successful today because my exam is going as well.
* For the next day, I have a plan to play basketball.

# 2021-12-29 #
* Having a exam on 14.10 Pm.
* At night, I have a group discussion for the next friday group presentation.
* Playing basketball at the night.
* I feel productive and successful today because the exam are going well and can play basketball.
* For the next day, I hope it will be a better day.

# 2021-12-30 #
* Today we are gonna to dress ourself with a formal uniform or clothes.
* We talk about our 5 rules to success in front of the class in a group.
* After saying the rules, our classmate are going to vote the best uniform or clothes in that group.
* I think to be success, we need to get enough sleep (more than 6 hours).
* Don't giveup and keep trying. I ever hear a word "Stop when you are done, not when you are tired".
* Take a break and have some fun for your mentall.