This diary file is written by YenHsun Wang E14076350 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-30 #

Three ways to spot a bad statistics

* Can you see uncertainty?
* Do you see yourself in the data?
* How was the data collected?

My experience in this class

* I just get clear on this course this week actually because i'm not good at English.
* I should pay more attention to follow the professor than in other classes.
* It is still worth it to stay focused while I got different vision in this calss!

# 2021-10-07 #

* I didn't know banks can create money through lending before.
* Banks create the money supply in most of the countries out of nothing through the proccess called credit creation.

# 2021-10-14 #

* Over time, fictional reality has become more and more powerful. 
* Sombody you think is undeserving of your compassion usually the ones who need it most.

# 2021-10-21 #

* There are three levels under what we think about. They are feel, emotion, and physiology.We need to control every levels to make us have good behavior.
* Doing exercise can strengthen memory, attention, even boost our mood.

# 2021-10-28 #

* Depression is super common in the world today. 7% of Americans experience depression in a year.
* Depression doesn't diminish a person's desire to connect with other people, just their ability.
* Just be friendly when you talk to a person who has depression.
* Having feelings isn't a sign of weakness.

# 2021-11-04 #

* Perhaps we make things up all the time.
* We make things up all the time.
* A lot of what we call self-knowledge is actually self-interpretation.

# 2021-11-25 #

* Creativity as an alternative to death is very real and true.
* It is not always the best way to let things free, although it is really convenient.It may make some trouble to people at the end.
* People need time to learn how to communicate with other people. It also costs time to make a deep connection with others.
* Nothing worth having comes easy.

# 2021-12-02 #

* I thought of my smartphone about 5 or 6 times during the class, mostly i wanted to know the time.	
  Sometimes i wanted to use the functions on the phone to help me work.
  I think i won't feel so anxious without my phone, but i might cannot let go when i'm playing on my smartphone.
* I found that we can absolutely aim high, but we can't forget what easy to do.
* In events like terrorist attacks and disasters, todays media attempts continuous coverage 
  even when no reliable new informatoin is available.
  It's easy to be anxious in such events.

# 2021-12-24 #

* 211223 Thu

         1. Unsuccessful and unproductive
         2. I felt unsuccessful because i overslept in the morning.
             I felt unproductive because i played with my smartphone almost the whole class.
         3. I will study with a friend at starbucks so that i won't be absent-minded.
          
* 211224 Fri

         1. Successful and unproductive
         2. I felt successful because i studied hard at starbucks this afternoon.
             I felt unproductive because i tried hard on a programing homework amd still not all finished before the deadline.
         3. I will get a good night's sleep anyway.
          
* 211226 Sun

         1. Unsuccessful and productive
         2. I felt unsuccessful because the coffee I made today is not so good.
            I felt productive because i did my homework early for this week.
         3. I will go jogging after class tomorrow so that i will be more sober.

* 211227 Mon

         1. Successful and unproductive
         2. I felt successful because i learned a lot in the German class.
            I felt unproductive because i spent too much time on a programing homework.
         3. I will do exercise for sure because i have PE class tomorrow.
         
* 211228 Tue

         1. Unsuccessful and unproductive
         2. I felt unseccussful because i overslept and i didn't hand in an assidnment.
            I felt unproductive because i can only do almost one assignment a night.
         3. I will do them earlier next time.
         
* 211229 Wed

         1. unsuccessful and productive 
         2. I felt unsuccessful because i am sleepy all day.
            I felt productive because i finished my homework quickly.
         3. I will get up on time to take the quiz.

* Five Rules

         1. Regular sleep
         2. Exercise three times a week
         3. Find a good place you work most efficiently

         
# 2021-12-30 #

* Other rules: Write diary every day, Reading habit
* The ideal of democracy is appealing but it is not feasible in practice.
* Diversity can trump ability (The microcosm of society).

# 2022-01-06 #


* In the video "What makes something go viral?", she say we usually care about things.

* But people get together not only curious about the things. People like to be involved, and participate in something together.

* It is the same on the internet. We will tag our friend on social media to see what we feel funny. 

* When we are tagged, we see the picture or video, and also think about the person who connect with us.

* So i think it is important to make connections with other people when we try to promote something or organise an event.