This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Winny Chang H24064064 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* I learned Wright's Law in the first class.（products will become cheaper from year to year）But I wonder if it is based on labor exploitation and environmental pollution. Do we still want it? 
* Fast fashion is an example. Do we need a lot? Do we need to chase the better one forever? 

* Glasl's model of conflict escalation. It let me know communication is important. Only communication can let us have a win-win situation.
* The professor gave other groups a lot of feedback. And I also made some errors they made in my ppt.
* I think I can learn a lot from this course.

# 2021-09-30 #

* I gave a presentation, and got some feedback from the professor. For example, it is not enough to cite the website, you also need to write the author's name and the last time you visited the website.
* My original teammates all dropped the course, and thanks for the TA, now I have two new teammates.
* There are three videos we need to watch this week, and my favorite one is "The best stats you've ever seen" by Hans Rosling. My major is statistics, and after studying for some years, I was bored with statistics, but after watching the video, it rebooted my passion for statistics. I hope I can be a statistician like Hank.
* The videos that professor always gave us always changed my mind a lot. I appreciate the professor's effort to the course and feel lucky to take the course.

# 2021-10-07 #
* I don't know many problems of the quiz that the teacher asked us to answer about the financial system.
* There is income inequality in Taiwan.
* It’s lucky to find my teammate easily this time.
* Although all my teammates are Taiwanese, we still  communicate in English. And it is a good way to practice English.
* I learned that the Gini index is an indicator to measure income inequality. The higher, the more inequality. 

# 2021-10-14 #

* 	financial system homework
	* 	workers need to ask for an adequate salary which should grow at the rate of inflation.
	* 	purchasing power should use the national dollar, and don't use the US dollar, because it will affect the inflation of the US dollar.
	* 	BigMac is good at comparing countries, but we just want to see the individual country.
	* 	what gives money its value: should be found by article, journal or textbook.
	* 	consumer debt/corporate debt should find its true values rather than its percent to GDP.
	* 	The country has a high balance sheet doesn't represent the country is rich. In stead, if it is too much, the country may have some financial problems. Because it means the central bank should buy many assets to make the price stable
* video
	* 	Every day, find somebody that you think is undeserving of your compassion and give it to them, because they're the ones who need it the most.
	* break the mirror that makes you see yourself better than you really are.
* English usage
	* 	compare apples and oranges means 把毫不相干的人或物硬湊在一起作比較.
* new instruction from teacher
	* 	record 2~4 minutes video to presentation
# 2021-10-21 #

* It is funny to hear that there is a myth in Malaysia that says Taiwanese English is proficient. 
* Exercise 3 to 4 times a week, 30 minutes.

# 2021-10-28 #

* I totally agree with “Being strong is killing us.”  I don’t need to be perfect, just be better than yesterday.
* Having feelings isn’t a sign of weakness. Feeling means we’re human.
* If my burden gets too heavy, I can ask for a hand. I can talk to the professors, our school therapists.

# 2021-11-04 #
* Video do you really know why you do what you do?
	* meaning is more to life than being happy.
	* meaning: 
		* belonging
		* purpose: not what you receive, but what you give
		* transcendecnce
		* storytelling: You can edit your life story, use another way to interpret your life story.
* For the course project, I  have learned how to go to NCKU library to find the master paper.


# 2021-11-18 #
* There is no class for this week.
* I forgot writing diary last week.
* Through the half of this semester, I think my English speaking skills is better than before.


# 2021-12-02 #

* This was the second time to have a physical class.

* And I finally had a physical meetup with my super group teammate.

* I had the second dose of the Moderna vaccine this weekend. I felt tired and had serious side effects.

# 2021-12-09 #

* I found that I had upload my diary last week but unfortuately it didn't upload successfully.
* The group project seems difficult. And we can't fininish it just one week.
* As the coming of being an exchange student in Finland, I have a lot of stuff to deal with like vaccine certificate.
* Taiwan certificate can not directly transfer to Eu covid passport!
* And I have to do some final report in advance.
* Though I have already got two shots of vaccine, I am still worried about Omicron. Hope Omicron will not be too harmful to us.


# 2021-12-16 #
* Teacher says Our team proposal for the course project is mission completed. Don't need to change the dormitary to give us more space to hang our clothes.
* And I change to the group 1 who want to change physical books to the pdf version.
* I'm unlucky this week.
* First, this week we need to change our mini group, and I found that two of my teammate has already dropped this class. So I am the only person of my group. But thanks to Kevin, allow me to join his group!
* I am going to fly to Finland in January. And the airline send me an email said that my flight was canceled. So they already change my flight 10 hours earlier than my oringinal flight which means I need to stay at the Hong Kong airport for 21 hours! I'm so sad.
* Not only my flight have been change, Finnish government decided to tighten the restrictions on entry to Finland, so I need 48 hours PCR test before arrival which was not needed before.

# 2021-12-23 #
* successful and productive.
* I felt successful because I do all of the work in my team this week. Despite of that,professor said I did a good job after I presented. Therefore, I’m really happy.
* I felt productive also because of I finished this week task on my own.

* Today I learned.
* Don’t pick a job. Pick a Boss.
* “Your first boss is the biggest factor in your career success. A boss who doesn’t trust you won’t give you opportunities to grow.” - William Raduchel
* “A good boss is better than a good company. A good boss would discipline you, train you, develop you.” - Jack Ma
* Next class should wear formal outfit.

# 2021-12-24 #
* successful and productive.
* I felt successful and productive because I finished a project which can let car track the road like autonomous car.
* Today is Chiristmas Eve.

# 2021-12-25 #
* successful and unproductive.
* I felt successful because I finally find a method that can let me successfully enter Finland in January!!!! Go todo PCR test in the morning of departure day.
* I felt unproductive because I have a meeting today. But we didn't discuss too much and someone was late for 30 minutes, so we wait for him a long time.

# 2021-12-26 #
* successful but unproductive.
* I felt successful because I finally have a free day. Just want relax, and don't do too much thing.
* I felt unproductive because I don't do too much thing just go to shop for the things that I needs in Finland.

# 2021-12-27 #
* unsuccessful and unproductive.
* I felt successful because I do nothing
* I felt unproductivy because I do nothing

# 2021-12-28 #
* unsuccessful and unproductive.
* I felt successful because I do nothing
* I felt unproductivy because I do nothing

# 2021-12-29 #
* successful and unproductive.
* I felt successful because I do some effort for our group project.


# 2021-12-30 #
* unsuccessful and unproductive.
* I felt unsuccessful because it is diffucult to implement our action before the end of the semester. And some of our teammates are not active for the group project.
* I felt unproductivy because I ask my previous professor who was the one that I think high probability will change his textbook from physical book to submit pdf to the moodle next semester. However, I'm wrong because he said he didn't print the book this semester, because most of students don't want to read and he thought that it is waste of paper. Although he is also do the thing which we want to but it is not because of us so I think it is not a successful example for our action.

# 2021-12-31 #
* unsuccessful and unproductive.
* I felt unsuccessful because I spend time with my family for celebrating new year at home, but after the dinner I got a cold from my brother.
* Because I got the cold, I felt sleepy and didn't have energy to do the schoolwork.

# 2022-01-01 #
* successful and unproductive.
* I felt successful because I got up early today but the first thing I do in the moring is to see a doctor. Doctor is the first person I saw in 2022. It seems not good.
* Because I got the cold, I felt sleepy and didn't have energy to do the schoolwork.

# 2022-01-02 #
* successful and unproductive.
* I felt successful because I prepare my luggage for studying in Finland and my cold get better than yesterday. Hope I will recover before boarding.
* Because I got the cold, I felt sleepy and didn't have energy to do the schoolwork.

# 2022-01-06 #
* I am in Finland now.
* It is so cold and it is my first time to see the snow. And the daytime is too short(only 9:30-16:30) 
* I have a lot of challenge to overcome.like:
* I still have a jet lag. 
* I need to cook by myself.
* I need to use English to communicate with others.
* I need to know the bus station and the market location.
* I need to buy daily necessities.
* I need to prepare the class in Finland next week.
* Also prepare the exam in Taiwan.

* Hope I can alive.

