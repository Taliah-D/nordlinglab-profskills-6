# 2021-02-16#
* introduction to the course
* we made groups based on numbers which was an interesting way of grouping
* We watched a video of elon musk and I learned that the cost of things will go down by a large scale in the future

# 2021-02-23#
* we watched a ted talk
* i learned that overtime the cost of solar panel decreased and the cost of nuclear energy went up, so the solar energy is way cheaper now
* the gap between presentations are so long, which takes time of other people that have to do their presentation

# 2021-09-30#
* I was very confused about the group thing
* liked the first ted talk, the second was not very useful
* more people presented today
# 2021-10-07
* when using an image for presentation, you have to write the date you visited the website
* the first break was funny
* realised that I know little about financial system
* learned that money's value comes from our belief in it
* didn't get time to watch the videos so we're watching on our own.
* poor people are the one's making the money for society
# 2021-10-21
* Talk about exercise health benefits. 
* No matter what time you exercise, you'll get the benefits anyway
* learned how the emotions play an important roll in education and can make you learn slower or faster.
* got confused about the grading system.
* we did an interesting experiment, and realised many people are stressed.
* fasting is not for everybody, depends on the individual.
* poor people are the one's making the money for society.
# 2021-10-14
* New things will be to start recording the presentations, which is a good idea so will let more people have the opportunity to present.
* i think it will be on youtube
* we create imaginary things that lives among us
* nationalism creates hate between humans
# 2021-10-28
* realised that it is easy to talk to people with depression, but the thing is that we  often feel responsible for their depression, so instead of talking to them we just try not to.
* happy people are often not as happy as we might thing, on the contrary, they could be masking up their emotions to deal with reality.
* "people can be sad and okay at the same time". Really liked that phrase.
* All it takes is to listen to what somebody has to say, you don't really need to say anything, just listen. 
* Also took the time to watch another ted talk from the survivor of the golden gate, the one mentioned by Kevin Briggs and I think is worth watching to understand more about the whole story.
# 2021-11-25
* I completely forgot to write on my diary for like three weeks because my mind was very occupied with all the midterms and reports to be done each week, and now I'm changing my strategy and will start to write the diary in class.
* Today I woke up late and could not attend the first class of the day, one about animal human history. 
* I do not know if only happens to me but I always receive the mails and moodle stuff so late, one day I received a mail from moodle to submit a report that was due for 2 days before.
* That is one the reasons why I came late to the class.
* Rene had to tell me that today we have physical class.
* I am worried about my grade on this course because it will have a huge impact on my final gpa and I need around 75 gpa to be able to transfer to another department.
* I have a problem with reading, that makes me have a hard time understanding when reading. For example, I have to ask my classmate what are we doing for this class because the tasks are vert confusing.
* I feel like the more sophisticated the words are, the more I get lost.
* I did not expect the class to be this cool, looks very futuristic and the screens helps with the seats and everything just looks cooler.
* Professor just said that we will start with presentations again and I think would help me develop public speaking skills, because I am not good at it.
* We watched a ted talk about the internet, about passions and supernatural models. 
* Ads became better.
* VR glasses are gonna get cheaper in the future, but our incomes might go down so It could be at the same price.
* Even those who come in the last place get a medal and it devaluates the value from it.
* we're good at showing people that our lives are perfect when in reality our lifes are shit. 
* living in the instragram and facebook world are making us feel worse.
* instagram is probably going to make youngsters feel discouraged and sad out of significant social applications, 
* instagram is one of the most harming web-based media in the world
* If you wake up and the first thing you do is use your phone before even say hi to your partner you clearly have a serious problem with addiction to social media.
* We care more about the year than the lifetime.
* I feel like since we started to use social media on a daily basis things are just getting worse and worse, because we became very dependent. Some people get really angry when they're not on their phone.
* I remember my grandma saying that I don't have to talk to her before she had her first cup of coffee in the morning and that is what an addict would say.
* When you remove the temptation it gets easier to not fall into the bad habit.
* Let the phone charging in another room because if you wake up in the middle of the night you won't be checking your phone.
* Also smart phones makes your night difficult because it interferes with your circadian rythm.
* We made group discussion and it was very funny.
* We called our group chess table because we had a chess board in our table. 
* I have never written this much but I feel like my grade is gonna go down, so I'm writting this long. Sorry
# 2021-12-09
* I wrote the last week diary but can't see it here
* finals are coming and honestly I am not prepared for it.
* just want this semester to end already
*　Ｂａｓｉｃａｌｌｙ　ｗｈａｔ　ｉ　ｓａｉｄ　ｉｎ　ｔｈｅ　ｏｔｈｅｒ　ｄｉａｒｙ　ｗａｓ　ｔｈａｔ　ｉ　ｆｉｎｄ　ｔｈｅ　ｉｎｓｔｒｕｃｔｉｏｎｓ　ｏｆ　ｔｈｅ　ｔａｓｋｓ　ｓｏ　ｄｉｆｆｉｃｕｌｔ　ｔｏ　ｕｎｄｅｒｓｔａｎｄ．
＊　ｍａｙｂｅ　ｉｔ＇ｓ　ｊｕｓｔ　ｍｅ　ｂｕｔ　ｉｄｋ．
# 2021-12-16
* Today we did a votacion (no me acuerdo como se decia en ingles), and I feel like we should really focus on the pdf thing.
* because the school will be a better place with pdf instead of the heavy and sometimes useless books.
*　to be honest, most of the books we buy are of no help, only works for solving the problems that the professor gives.
* like, this edition has new problems and shit.
* so changing to pdf would mean that; first, our backs will be saved because of the heavy weights, and our pockets too.
* the university has to provide pdf for everyone, but of course that's not gonna happen :(
* Tomorrow is my applied mechanics test, hope to do it better than the last two exams.
* Hopefully this time my diary will stay and NOT disappear like last time >:( xdd
# 2021-12-23
* today i didnt go to class because I was feeling so bad due to the lack of sleep, but fortunately my friend told me what we have to do.
* I have not been productive today, because I was too tired to make decisions.
* so I had to take a full 5 hour of sleep to get back in track and now I'm finishing a project and studying for tomorrow's exam.
* unfortunately it is gonna ruin my sleep schedule but that's okay if I get to pass the class.
* So today was not a successful day
# 2021-12-24
* not a productive day
# 2021-12-25
* not a productive day and forgot to write the diary
# 2021-12-26
* today was a productive day, woke up early and finished my tasks so early that I could go out and enjoy.

<<<<<<< HEAD
# 2021-12-02
* We did a little experiment in class today.
* professor basically took our phones
* I don't feel like I want to get my phone back during the class time.
* We are gonna save at least 10 people, we're superheroes.
* Closing the church could be a good idea :'D jk
* Construction noice is worse.
* The government have to pay us because every night we have to hear old people sing bible stuff xd
* Today I'm gonna write a bit shorter than the last time
=======
# 2021-12-30
* unsuccessful because i failed to write the diary again
* also because I didnt do any school homework 
# 2021-12-31
* last day of the year yay!
* successful but not productive
* because I didnt do any homework but I got a great workout, that for me is very important
# 2022.1.1
* unproductive because I only watched the spiderman series
* successful cause I worked out
>>>>>>> 3e724ad633b242adbfb71ab11271897cb714b8ac
# 2022.1.2
* unsuccessful because didn't do much
# 2022.01.03
* today's my birthday lol
* very productive cause I've finished an annoying essay
# 2022.01.04
* today is gonna be the day that they're gonna throw it back tooyouuuuuuuuu
* today was very productive, woke up early and did everything by 12 pm
* next week the 5 days i got shit to do and its so stressing
* to be honest, this semester was awful. made bad decisions in choosing classes and didn't have time to drop it because I was focusing on the harder courses.
* I'm concerned about my grades, cause this semester's grades are important for me to transfer to another department
*　Because I also chose the wrong department lol, but I guess is how life works and yeah.
* I'm excited for winter break cause then I could go to the mountains and feel alive again.
# 2022.01.05
* not a productive day cause I couldn't sleep last night so woke up with bad temper today
* winter is coming!!!
# 06-01-2022
* for some reason this class 的 grades makes me feel so uncomfortable, because it's very unsure and I need a good gpa in order to transfer
* i dont know if we are still doing the weekly thing
* i have a final next friday and my life depends on that test
